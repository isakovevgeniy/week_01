import org.junit.Assert;
import org.junit.Test;
import ru.edu.lesson1.GreetingImpl;
import ru.edu.lesson1.IGreeting;

public class GreetingsTests {
    static private GreetingImpl greetingImpl = GreetingImpl.getIvanovII();

    @Test
    public void size_Test() {
        Assert.assertNotNull(greetingImpl);
    }

    @Test
    public void tostring() {
        Assert.assertNotNull(greetingImpl.toString());
        System.out.println(greetingImpl);
    }

    @Test
    public void getFirstName() {
        Assert.assertEquals("Ivan", greetingImpl.getFirstName());
    }

    @Test
    public void getSecondName() {
        Assert.assertEquals("Ivanovich", greetingImpl.getSecondName());
    }

    @Test
    public void getLastName() {
        Assert.assertEquals("Ivanov", greetingImpl.getLastName());
    }

    @Test
    public void getHobbies() {
        Assert.assertNotNull(greetingImpl.getHobbies());
    }

    @Test
    public void getBitbucketUrl() {
        Assert.assertEquals("https://bitbucket.org/isakovevgeniy/", greetingImpl.getBitbucketUrl());
    }

    @Test
    public void getPhone() {
        Assert.assertEquals("+7(000)1324567", greetingImpl.getPhone());
    }

    @Test
    public void getCourseExpectation() {
        Assert.assertNotNull(greetingImpl.getCourseExpectation());
    }

    @Test
    public void getEducationInfo() {
        Assert.assertNotNull(greetingImpl.getEducationInfo());
    }
}
