package ru.edu.lesson1;

public class Hobby {
    final String id;
    final String name;
    final String description;

    public Hobby (String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Hobby (String id, String name) {
        this.id = id;
        this.name = name;
        this.description = "";
    }
}
