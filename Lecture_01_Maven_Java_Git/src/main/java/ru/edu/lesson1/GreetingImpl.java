package ru.edu.lesson1;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class GreetingImpl implements IGreeting {
    private static GreetingImpl IvanovII;

    final String firstName = "Ivan";
    final String secondName = "Ivanovich";
    final String lastName = "Ivanov";
    Set<Hobby> hobbies = new HashSet<>();
    String bitbucketUrl;
    String phone;
    String courseExpectation;
    String educationInfo;

    private GreetingImpl() {

        phone = "+7(000)1324567";
        educationInfo = "University";
        courseExpectation = "Giant hopes";
        bitbucketUrl = "https://bitbucket.org/isakovevgeniy/";
        hobbies.add(new Hobby("1", "SnowBoarding"));
        hobbies.add(new Hobby("2", "Learning Java"));
    }

    public static GreetingImpl getIvanovII() {
        if (IvanovII == null)
            synchronized (GreetingImpl.class) {
                if (IvanovII == null)
                    IvanovII = new GreetingImpl();
            }
        return IvanovII;
    }

    @Override
    public String toString() {
        String basicInfo = String.format(
                "First name: %s%n" +
                        "Second name: %s%n" +
                        "Last name: %s%n" +
                        "Phone number: %s%n" +
                        "Education: %s%n" +
                        "Expectation of the course: %s%n",
                firstName,
                secondName,
                lastName,
                phone,
                educationInfo,
                courseExpectation);
        String hobbiesInfo = "Hobbies: ";
        for (Hobby str : hobbies)
            hobbiesInfo += " " + str.name + ";";
        return basicInfo + hobbiesInfo;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getSecondName() {
        return secondName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public Collection<Hobby> getHobbies() {
        return hobbies;
    }

    @Override
    public String getBitbucketUrl() {
        return bitbucketUrl;
    }

    @Override
    public String getPhone() {
        return phone;
    }

    @Override
    public String getCourseExpectation() {
        return courseExpectation;
    }

    @Override
    public String getEducationInfo() {
        return educationInfo;
    }
}
