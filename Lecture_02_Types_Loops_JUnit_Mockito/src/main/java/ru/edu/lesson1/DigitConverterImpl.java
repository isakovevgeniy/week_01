package ru.edu.lesson1;

import java.util.Locale;

public class DigitConverterImpl implements DigitConverterHard{
    /**
     * Конвертация целой части в выбранную систему счисления.
     * @param digit - число
     * @param radix - основание системы счисления
     * @return result - представление числа в выбранной системе счисления в виде строки
     */
    @Override
    public String convert(int digit, int radix) {
        checkArguments(digit, radix);

        StringBuilder result = new StringBuilder();
        do {
            result.insert(0,digit % radix);
            digit = digit/radix;
        } while (digit != 0);

        return result.toString();
    }

    /**
     *  Конвертация дробной части в выбранную систему счисления.
     * @param digit     - число
     * @param radix     - основание системы счисления
     * @param precision - точность, сколько знаков после '.'
     * @return result - представление числа в выбранной системе счисления в виде строки
     */
    public String convert(double digit, int radix, int precision) {
        checkArguments(digit, radix, precision);

        if (radix == 10) return String.format(Locale.US,"%.5f", digit);

        String integerPart = convert((int)digit, radix);
        StringBuilder decimalPart = new StringBuilder();

        double decimalFractionOfDigit = digit - (int) digit;
        int tempRadix = radix;

        for (int i = 0; i < precision; i++) {
            double a = 1.0/tempRadix;

            if (decimalFractionOfDigit - a < 0) {
                decimalPart.append("0");
            } else {
                int k = 0;
                while (decimalFractionOfDigit - a >= 0) {
                    decimalFractionOfDigit = decimalFractionOfDigit - a;
                    k++;
                }
                decimalPart.append(k);
            }
            tempRadix *= radix;
        }

        int length = precision - decimalPart.length();
        for (int i = 0; i < length; i++)
            decimalPart.append("0");
        return integerPart + "." + decimalPart;
    }

    /**
     * Проверить параметры, выбросить исключение.
     * @param digit     - число
     * @param radix     - основание системы счисления
     */
    void checkArguments(double digit, int radix){
            if (radix < 1 || digit < 0) throw new IllegalArgumentException();
    }

    /**
     * Проверить параметры, выбросить исключение.
     * @param digit     - число
     * @param radix     - основание системы счисления
     * @param precision - точность, сколько знаков после '.'
     */
    void checkArguments(double digit, int radix, int precision) {
        if (radix < 1 || digit < 0 || precision <= 0) throw new IllegalArgumentException();
    }
}
